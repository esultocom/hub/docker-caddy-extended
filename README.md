[![pipeline status](https://gitlab.com/esultocom/hub/docker-caddy-extended/badges/main/pipeline.svg)](https://gitlab.com/esultocom/hub/docker-caddy-extended/-/commits/main) [![coverage report](https://gitlab.com/esultocom/hub/docker-caddy-extended/badges/main/coverage.svg)](https://gitlab.com/esultocom/hub/docker-caddy-extended/-/commits/main)

# Caddy Docker custom image

[Caddy](https://github.com/caddyserver/caddy) Docker image extended with extra modules

# Modules included

- [caddy-l4](https://github.com/mholt/caddy-l4)
  Enables Caddy to be used as a reverse proxy for raw TCP/UDP connections.

- [caddy-docker-proxy](https://github.com/lucaslorentz/caddy-docker-proxy/plugin/v2)
  Enables Caddy to be used as a reverse proxy for Docker containers via labels.

- [caddy-tlsredis](https://github.com/gamalan/caddy-tlsredis)
  Enables Caddy to used Redis storage

- [certmagic-s3](https://github.com/ss098/certmagic-s3)
  Enables Caddy to use S3 or Minio storage

### Please find LICENSE link for caddy below 

[Caddy license](https://github.com/caddyserver/caddy/blob/master/LICENSE)