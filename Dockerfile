FROM caddy:2.4.5-builder-alpine AS builder

RUN xcaddy build \
  --with github.com/mholt/caddy-l4 \
  --with github.com/lucaslorentz/caddy-docker-proxy/plugin \
  --with github.com/gamalan/caddy-tlsredis@v0.2.7 \
  --with github.com/ss098/certmagic-s3

FROM caddy:2.4.5-alpine

COPY --from=builder /usr/bin/caddy /usr/bin/caddy

CMD ["caddy", "docker-proxy"]